import os
import requests

BASE_URL = "https://gitlab.com/api/v3"

def login():
  """Login a uste into GitLab""" 
  return requests.post(BASE_URL+'/session?login='+os.environ['GITLAB_USER']+'&password='+os.environ['GITLAB_PW'])

def project_list():
  """Lists all projects for logged user"""
  headers = {'PRIVATE-TOKEN':os.environ['GITLAB_TOKEN']}
  return requests.get(BASE_URL+'/projects', headers=headers)
